package main

import (
	"fmt"
)

func main() {
	var n int
	fmt.Scanf("%d", &n)

	var input int
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &input)
		if input%2 == 0 {
			fmt.Printf("%d is even\n", input)
		} else {
			fmt.Printf("%d is odd\n", input)
		}
	}
}
