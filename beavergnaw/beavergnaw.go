package main

import (
	"fmt"
	"math"
)

func main() {
	var D, V int

	fmt.Scanf("%d %d", &D, &V)
	for D != 0 && V != 0 {
		var d float64
		d = math.Pow(float64(D*D*D)-(6.0*float64(V)/math.Pi), 1.0/3.0)
		fmt.Printf("%.9f\n", d)
		fmt.Scanf("%d %d", &D, &V)
	}
}
