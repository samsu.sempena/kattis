package main

import (
	"fmt"
	"io"
)

func main() {
	for {
		var a, b int
		_, err := fmt.Scanf("%d %d", &a, &b)
		if err == io.EOF {
			break
		}

		diff := a - b
		if diff < 0 {
			diff = diff * -1
		}
		fmt.Printf("%d\n", diff)
	}
}
