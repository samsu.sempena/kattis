package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	fmt.Scanf("%d", &n)

	var result int
	var input int
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &input)
		result = result + int(math.Pow(float64(input/10), float64(input%10)))
	}

	fmt.Printf("%d", result)

}
