package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	var N int
	fmt.Scanf("%d", &N)

	scanner := bufio.NewScanner(os.Stdin)
	var name string

	var count int
	for scanner.Scan() {
		name = scanner.Text()
		name = strings.ToLower(name)

		if strings.Contains(name, "rose") || strings.Contains(name, "pink") {
			count = count + 1
		}
	}

	if count > 0 {
		fmt.Printf("%d", count)
	} else {
		fmt.Printf("I must watch Star Wars with my daughter")
	}
}
