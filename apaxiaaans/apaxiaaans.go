package main

import (
	"fmt"
	"strings"
)

func main() {
	var name string
	fmt.Scanf("%s", &name)

	var sb strings.Builder
	var prevChar rune 
	for _, char := range name {
		if char != prevChar {
			sb.WriteRune(char)
		} 
		prevChar = char
	}
	fmt.Printf("%s", sb.String())
}
