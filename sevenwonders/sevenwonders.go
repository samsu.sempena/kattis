package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode/utf8"
	"math"
)

func main() {
	var char rune

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanRunes)

	var T, C, G, score int

	for scanner.Scan() {
		char, _ = utf8.DecodeRune(scanner.Bytes())
		//fmt.Printf("%s", string(char))

		if char == 'T' {
			T++
		} else if char =='C' {
			C++
		} else if char == 'G' {
			G++
		} 
	}

	score = T*T + C*C + G*G + 7 * int(math.Min(math.Min(float64(T), float64(C)), float64(G)))

	fmt.Printf("%d\n", score)
}
