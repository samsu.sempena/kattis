package main

import (
	"fmt"
)

func main() {
	var L, R int

	fmt.Scanf("%d %d", &L, &R)
	if L == 0 && R == 0 {
		fmt.Printf("Not a moose")
	} else if L == R {
		fmt.Printf("Even %d", L+R)
	} else {
		if L > R {
			fmt.Printf("Odd %d", L*2)
		} else {
			fmt.Printf("Odd %d", R*2)
		}
	}
}
