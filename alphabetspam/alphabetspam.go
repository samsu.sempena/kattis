package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode"
	"unicode/utf8"
)

func main() {
	var char rune

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanRunes)
	var isWhitespace, isLowercase, isUppercase, isSymbol, total int

	for scanner.Scan() {
		char, _ = utf8.DecodeRune(scanner.Bytes())
		//fmt.Printf("%s", string(char))

		if char == '_' {
			isWhitespace++
		} else if unicode.IsLower(char) {
			isLowercase++
		} else if unicode.IsUpper(char) {
			isUppercase++
		} else {
			isSymbol++
		}
		total++
	}
	isSymbol--
	total--

	fmt.Printf("%.10f\n", float64(isWhitespace)/float64(total))
	fmt.Printf("%.10f\n", float64(isLowercase)/float64(total))
	fmt.Printf("%.10f\n", float64(isUppercase)/float64(total))
	fmt.Printf("%.10f\n", float64(isSymbol)/float64(total))
}
