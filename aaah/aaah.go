package main

import (
	"fmt"
)

func main() {
	var ability, expectation string
	fmt.Scanf("%s", &ability)
	fmt.Scanf("%s", &expectation)

	if len(ability) < len(expectation) {
		fmt.Printf("no")
	} else {
		fmt.Printf("go")
	}
}
