package main

import (
	"fmt"
	"strings"
	"unicode"
)

func main() {
	var name string
	fmt.Scanf("%s", &name)

	var sb strings.Builder
	for _, char := range name {
		if unicode.IsUpper(char) {
			sb.WriteRune(char)
		}
	}
	fmt.Printf("%s", sb.String())
}
