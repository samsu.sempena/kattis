package main

import (
	"fmt"
	"strings"
)

func main() {
	var word string
	fmt.Scanf("%s", &word)

	if strings.Contains(word, "ss") {
		fmt.Printf("hiss")
	} else {
		fmt.Printf("no hiss")

	}
}
