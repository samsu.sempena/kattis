package main

import (
	"fmt"
)

func main() {
	var N int

	fmt.Scanf("%d", &N)
	for (N != -1) {
		var S, prevT, T, total int
		for i:=0; i<N;i++ {
			fmt.Scanf("%d %d", &S, &T)
			total = total + S* (T - prevT)
			prevT = T
		}
		fmt.Printf("%d miles\n", total)
		fmt.Scanf("%d", &N)
	}

}
