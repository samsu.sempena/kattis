package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	var name string

	for scanner.Scan() {
		name = scanner.Text()

		if name == "OCT 31" || name == "DEC 25" {
			fmt.Printf("yup")
		} else {
			fmt.Printf("nope")
		}
	}

}
