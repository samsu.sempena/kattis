package main

import (
	"fmt"
)

func main() {
	var L, x int
	fmt.Scanf("%d %d", &L, &x)

	var peopleInside, rejectedCount int

	for i := 0; i < x; i++ {
		var event string
		var count int

		fmt.Scanf("%s %d", &event, &count)

		if event == "enter" {
			if peopleInside+count <= L {
				peopleInside = peopleInside + count
			} else {
				rejectedCount = rejectedCount + 1
			}
		} else if event == "leave" {
			peopleInside = peopleInside - count
		}
	}

	fmt.Printf("%d", rejectedCount)
}
