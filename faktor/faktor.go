package main

import (
	"fmt"
)

func main() {
	var n1, n2 int
	fmt.Scanf("%d %d", &n1, &n2)

	fmt.Printf("%d", n1*(n2-1)+1)

}
