package main

import (
	"fmt"
)

func main() {
	var cost float64
	fmt.Scanf("%f", &cost)

	var N int
	fmt.Scanf("%d", &N)

	var total float64
	for i := 0; i < N; i++ {
		var width, length float64
		fmt.Scanf("%f %f", &width, &length)
		total = total + width*length*cost
	}

	fmt.Printf("%.7f", total)
}
