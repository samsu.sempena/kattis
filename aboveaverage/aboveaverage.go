package main

import (
	"fmt"
)

func main() {
	var T int
	fmt.Scanf("%d", &T)
	for i := 0; i < T; i++ {
		var N int
		fmt.Scanf("%d", &N)

		data := make([]int, N)
		var sum int
		for j := 0; j < N; j++ {
			fmt.Scanf("%d", &data[j])
			sum = sum + data[j]
		}
		avg := sum / N

		aboveavg := 0
		for j := 0; j < N; j++ {
			if data[j] > avg {
				aboveavg++
			}
		}

		result := float64(aboveavg) / float64(N) * 100.0
		fmt.Printf("%.3f%s\n", result, "%")
	}
}
