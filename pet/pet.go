package main

import (
	"fmt"
)

func main() {
	var r1, r2, r3, r4, totalMax, total, winner int

	for i := 1; i <= 5; i++ {
		fmt.Scanf("%d %d %d %d", &r1, &r2, &r3, &r4)
		total = r1 + r2 + r3 + r4

		if total > totalMax {
			totalMax = total
			winner = i
		}
	}
	fmt.Printf("%d %d", winner, totalMax)
}
