package main

import (
	"fmt"
	"sort"
)

func main() {
	var n1, n2, n3 int
	var order string
	fmt.Scanf("%d %d %d", &n1, &n2, &n3)
	fmt.Scanf("%s", &order)

	n := []int{n1, n2, n3}
	sort.Ints(n)

	switch order {
	case "ABC":
		fmt.Printf("%d %d %d", n[0], n[1], n[2])
	case "ACB":
		fmt.Printf("%d %d %d", n[0], n[2], n[1])
	case "BAC":
		fmt.Printf("%d %d %d", n[1], n[0], n[2])
	case "BCA":
		fmt.Printf("%d %d %d", n[1], n[2], n[0])
	case "CAB":
		fmt.Printf("%d %d %d", n[2], n[0], n[1])
	case "CBA":
		fmt.Printf("%d %d %d", n[2], n[1], n[0])
	default:
		fmt.Printf("N/A")
	}

}
