package main

import (
	"fmt"
)

func reverse(number int) int {
	remainder := 0
	sumResult := 0
	for number != 0 {
		remainder = number % 2
		number = number / 2
		sumResult = sumResult*2 + remainder
	}
	return sumResult
}

func main() {
	var T int
	fmt.Scanf("%d", &T)
	fmt.Printf("%d", reverse(T))
}
