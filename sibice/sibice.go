package main

import (
	"fmt"
)

func main() {
	var n, w, h int
	fmt.Scanf("%d %d %d", &n, &w, &h)

	var length, input int
	length = w*w + h*h

	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &input)
		if input*input <= length {
			fmt.Println("DA")
		} else {
			fmt.Println("NE")
		}
	}
}
