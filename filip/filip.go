package main

import (
	"fmt"
)

func flip(n int) int {
	var flipped int

	for n > 0 {
		flipped = (flipped + n%10) * 10
		n = n / 10
	}
	return flipped / 10
}

func main() {
	var n1, n2 int
	fmt.Scanf("%d %d", &n1, &n2)

	fn1 := flip(n1)
	fn2 := flip(n2)

	if fn1 > fn2 {
		fmt.Printf("%d", fn1)
	} else {
		fmt.Printf("%d", fn2)
	}
}
