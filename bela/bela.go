package main

import (
	"fmt"
)

func main() {
	var N int
	var B string

	fmt.Scanf("%d %s", &N, &B)

	var total int
	for i := 0; i < N*4; i++ {
		var card string
		fmt.Scanf("%s", &card)

		if rune(card[1]) == rune(B[0]) {
			// dominant
			switch card[0] {
			case 'A':
				total = total + 11
			case 'K':
				total = total + 4
			case 'Q':
				total = total + 3
			case 'J':
				total = total + 20
			case 'T':
				total = total + 10
			case '9':
				total = total + 14
			case '8':
				total = total + 0
			case '7':
				total = total + 0
			}
		} else {
			switch card[0] {
			case 'A':
				total = total + 11
			case 'K':
				total = total + 4
			case 'Q':
				total = total + 3
			case 'J':
				total = total + 2
			case 'T':
				total = total + 10
			case '9':
				total = total + 0
			case '8':
				total = total + 0
			case '7':
				total = total + 0
			}

		}
	}

	fmt.Printf("%d", total)
}
