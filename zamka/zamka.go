package main

import (
	"fmt"
)

func sumDigits(number int) int {
	remainder := 0
	sumResult := 0
	for number != 0 {
		remainder = number % 10
		sumResult += remainder
		number = number / 10
	}
	return sumResult
}

func main() {
	var L, D, X int
	fmt.Scanf("%d", &L)
	fmt.Scanf("%d", &D)
	fmt.Scanf("%d", &X)

	for i := L; i <= D; i++ {
		if sumDigits(i) == X {
			fmt.Println(i)
			break
		}
	}

	for i := D; i >= L; i-- {
		if sumDigits(i) == X {
			fmt.Println(i)
			break
		}
	}
}
