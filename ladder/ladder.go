package main

import (
	"fmt"
	"math"
)

func main() {
	var h, v int

	fmt.Scanf("%d %d", &h, &v)
	fmt.Printf("%d", int(math.Ceil(float64(h)/math.Sin(float64(v)*math.Pi/180))))
}
