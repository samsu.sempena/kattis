package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	var name string

	for scanner.Scan() {
		name = scanner.Text()
		var sb strings.Builder
		var char rune

		for i := 0; i < len(name); i++ {
			char = rune(name[i])
			sb.WriteRune(char)
			if char == 'a' || char == 'i' || char == 'u' || char == 'e' || char == 'o' {
				i = i + 2
			}
		}
		fmt.Printf("%s", sb.String())
	}

}
