package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	var N int
	fmt.Scanf("%d", &N)

	scanner := bufio.NewScanner(os.Stdin)
	var string1, string2 string

	for scanner.Scan() {
		string1 = scanner.Text()
		scanner.Scan()
		string2 = scanner.Text()

		var sb strings.Builder
		var char1, char2 rune

		for i := 0; i < len(string1); i++ {
			char1 = rune(string1[i])
			char2 = rune(string2[i])

			if char1 == char2 {
				sb.WriteRune('.')
			} else {
				sb.WriteRune('*')
			}
		}

		fmt.Printf("%s\n", string1)
		fmt.Printf("%s\n", string2)
		fmt.Printf("%s\n\n", sb.String())
	}

}
