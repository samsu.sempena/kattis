package main

import (
	"fmt"
)

func main() {
	var n1, n2, n3 int
	fmt.Scanf("%d %d", &n1, &n2)

	if n1 > n2 {
		n3 = n2
		n2 = n1
		n1 = n3
	}

	for i := n1 + 1; i <= n2+1; i++ {
		fmt.Printf("%d\n", i)
	}

}
