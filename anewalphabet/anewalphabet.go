package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strings"
)

func main() {
	var input string
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		input = scanner.Text()
		input = strings.ToLower(input)

		var newalphabet map[rune]string
		newalphabet = make(map[rune]string)

		newalphabet['a'] = "@"
		newalphabet['b'] = "8"
		newalphabet['c'] = "("
		newalphabet['d'] = "|)"
		newalphabet['e'] = "3"
		newalphabet['f'] = "#"
		newalphabet['g'] = "6"
		newalphabet['h'] = "[-]"
		newalphabet['i'] = "|"
		newalphabet['j'] = "_|"
		newalphabet['k'] = "|<"
		newalphabet['l'] = "1"
		newalphabet['m'] = "[]\\/[]"
		newalphabet['n'] = "[]\\[]"
		newalphabet['o'] = "0"
		newalphabet['p'] = "|D"
		newalphabet['q'] = "(,)"
		newalphabet['r'] = "|Z"
		newalphabet['s'] = "$"
		newalphabet['t'] = "']['"
		newalphabet['u'] = "|_|"
		newalphabet['v'] = "\\/"
		newalphabet['w'] = "\\/\\/"
		newalphabet['x'] = "}{"
		newalphabet['y'] = "`/"
		newalphabet['z'] = "2"

		var b bytes.Buffer

		for _, char := range input {
			if val, ok := newalphabet[char]; ok {
				b.WriteString(val)
			} else {
				b.WriteString(string(char))
			}
		}

		fmt.Printf("%s", b.String())
	}
}
